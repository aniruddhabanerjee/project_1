const problem1 = require('./problem1')
const inventory = require('./inventory')

const result = problem1(inventory, 33)
console.log(result)
console.log(
  `Car ${result[0].id} is a ${result[0].car_year} ${result[0].car_make} ${result[0].car_model}`
)
